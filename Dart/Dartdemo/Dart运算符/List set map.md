 List 里常用的属性和方法

 常用属性：
    length 长度
    reversed  翻转
    isEmpty 是否不空
    1sNotEmpty 是否不为空

常用方法
    add     增加
    addAII  拼接数组    
    indexof 查找 传入具体值
    remove  删除传入具体值
    removeAt    删除传入索引
    fillRange   修改
    insert (index,value) 插入指定位置
    insertAll（index ,list） 指定插入Litst
    toList()    其他类型装换成List
    join()    List 转换成字符串
    split()     字符串装换成 List
    forEach
    map
    where
    any
    every