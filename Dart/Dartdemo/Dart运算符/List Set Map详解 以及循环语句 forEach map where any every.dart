/**
  List 里常用的属性和方法

 常用属性：
    length 长度
    reversed  翻转
    isEmpty 是否不空
    1sNotEmpty 是否不为空

常用方法
    add     增加
    addAII  拼接数组    
    indexof 查找 传入具体值
    remove  删除传入具体值
    removeAt    删除传入索引
    fillRange   修改
    insert (index,value) 插入指定位置
    insertAll（index ,list） 指定插入Litst
    toList()    其他类型装换成List
    join()    List 转换成字符串
    split()     字符串装换成 List
    forEach
    map
    where
    any  m满足一个就返回true
    every  全部满足才返回ture
 */


void list(List<String> args) {
  //第一中
  // List mylist = ['red','getrs','pink'];
  // print(mylist[0]);
  //第二
  var list = new List<String>(); // 定义一个为string的数组

  list.add('99');
  list.add('88');
  list.add('77');
  list.add('66');
  list.add('55');

// print(list);

print(list.length);
print(list.isEmpty);
print(list.isNotEmpty);
print(list.reversed);
print(list.reversed.toList());

print(list.join('*'));

list.add('ppp');

list.addAll(['aa','bb','cc','dd']);


list.remove('99');//根据值来删除
list.removeAt(5);//根据索引删除
print(list);
print(list.join('&'));
// print(list);



}

void set(List<String> args) {
  
  // 类似与js的set（）
  var set;
  set = new Set();
  set.add('苹果');
  set.add('香蕉');
  set.add('香蕉');
  set.add('瞒过');
  print(set);
  print(set.toString());
  print(set.toList());


  
  var set1 = new List();
  set1.add('苹果');
  set1.add('香蕉');
  set1.add('香蕉');
  set1.add('瞒过');

  print(set1);
  print(set1.toSet());

}

void Maps(List<String> args) {
  /**
   
映射（Maps）是无需的键值对

常用属性：

    keys  获取所有的key值
    values  获取所有的values值
    isEmpty 是否为空
    inNotEmpty  是否非空


常用方法
    remove(key) 删除指定的key值
    addAll({...}) 合并映射 给映射内容添加属性
    cottainsValue   查看映射内的值 返回 true/false
    forEach
    map
    where
    any
    every

   */


  // Map obj = {'name':'huangju','age':22,"sex":'nv'};

  // var po = new Map();
  // po['name'] = 'Lg';
  // print(obj);
  // print(po);

  Map obj = {'name':'huangju','age':22,"sex":'nv'};
  obj.addAll({'wrog':['护士','打针','拿药'],'width':1080}); 
  print(obj.keys);
  obj.remove('wrog');
  print(obj.values);
  print(obj.containsValue(1080));  //查看maps 集合中是否存在
}

void main(List<String> args) {
  
  List mylist = ['苹果','香蕉','桃子','葡萄','芒果','🍌'];

  mylist.forEach((e){print(e);});

  List num = [2,4,6,8];
  var numcont = new List();
  numcont=num.map((e){return e*2;});

  // print(numcont);
  // print(numcont.toList());
  // print(numcont.join('-'));
  // print(numcont.toString());

  List Nubuer = [1,5,8,0,9,4];

  var mylis = Nubuer.where((value){return value > 5;});

  print(mylis);
  print(mylis.toString());
  print(mylis.toList());
  // print(mylis);


var mm= mylis.any((e){return e>8;});
print(mm);

var mm1= mylis.every((e){return e>1;});
print(mm1);
}