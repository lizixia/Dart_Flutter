main() {
  
String str = '12';

var myint = int.parse(str);  //转为int 类型

// print(myint is int);

String mydabo = '89.99';

var mydaob = double.parse(mydabo);

// print(mydaob is double);

/*
//错误
String pic = '';

var myNub = int.parse(pic);

print(myNub);

//错误
String pic = '';

pic??= '0';

print(pic);
*/

String mk = '';
try {
  
  var mok = int.parse(mk);
  print(mok);

  print(mok is int);

} catch (e) {

  print(0);

}

int myints = 99;

var mkstr = myints.toString();// 转化为 字符串

print(mkstr);
print(mkstr is String);


///isEmpty  是否为空关键字

var mgsm = '';

if(mgsm.isEmpty){
  print('空');
}else{
  print('非空');

}

var mks = 0/0;


if(mks.isNaN){
  print('nan');
}

}