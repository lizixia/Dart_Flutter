// main(){

//   print('你好');
//   print('holl word');
// }

void main() {
  String mo = '静安寺大家';

  int nub = 21;
  print(nub);

  var str = 'this .in str';
// str = 'string 类型'
// print(str)

//常量定义

// const ip =3.14159;

// final

//字符串的拼接  '$变量名 $变量名' 也可 '变量名 + 变量名'
  String str1 = '你好';

  String str2 = 'Dart';

  print("$str1 $str2");

//数字类型
//int 必须是整型
  int a = 123;

  print(a);
//double 可是整型 也可是浮点行
  double b = 23.5;
  print(b + a);

//布尔值
  bool f = false;

  print(f);

  bool i = true;

  if (i) {
    print(i);
  }
  var li = ['aa', 'bb', 'vv'];

  print(li[2]);

  var l1 = new List();
  l1.add('1');
  l1.add('2');
  l1.add('3');
  l1.add('4');

  print(l1);

//对象的定义

  var obj = {
    'name': 'Dart',
    'age': 2001,
    'sex': 'na',
    'jn': ['javasrpit', 'typescrpt', 'vue']
  };

  print(obj);
//Datr中使用对象中的值
  print(obj['age']);
  print(obj['jn']);

  var p = new Map();
  p['name'] = 'DART';
  p['sex'] = 18;
  p['wro']=['外卖','程序'];
  print(p);

// 第二种maps 定义

var ps = new Map();
ps['name']='刘忠总是';
ps['sex']=18;

print(ps);


  //Dart 类型判断

  var st = 123;

  if(st is int){

    print(st is int);
  }

// var li1 = new List<String>();

// li1.add('ll');
// li1.add('bb');
// li1.add('vv');
// li1.add('ff');
// li1.add('lddl');

// print(li1);



// var li1 = new List<Object>();

// li1.add(12);
// li1.add('bb');
// li1.add(false);
// li1.add('ff');
// li1.add(123.20);

// print(li1);




}
