dart 语法

所有的运行代码都在 main 里面

dart 中有3中定义变量的方式

Dart 中所有执行的代码都在 main(){}函数中

void main(){} 代表没有返回值


输出   print(123);

注释 /**/  //       ///

var   任意类型  会自动推动变量的类型  但是var本身并不是一种类型，var声明的变量在赋值的那一刻，就已经决定了它是什么类型了
String   字符串类型
int  数字类型

var num = 44; 
num = 'hello';//编译错误  Error: A value of type 'dart.core::String' can't be assigned to a variable of type 'dart.core::int'.
如果对象不限于单一类型（没有明确的类型），请使用Object或dynamic关键字。


Object num = 44; 
num = 'hello'; //允许动态变更类型

dynamic num2 = 44; 
num2 = 'hello';  //允许动态变更类型

Object和dynamic的区别
Object之所以能够被赋值为任意类型的原因，其实都知道，因为所有的类型都派生自Object，它可以赋值为任何类型。由于Object是一个类，并不支持某些数学运算符的操作，因此下面的代码在编译时会报错：


Object num = "44"; 
num++; //编译错误





默认值
没有初始化的变量自动获取一个默认值为 null。即使是基本类型(int、long、short)如何没有初始化其值也是 null，不要忘记了基本类型也是对象:

int lineCount;
  
if(lineCount == null){
    print(lineCount);//打印null
}


dart 定义变量 有大小写区分    

Final 和 const（常量）



    字符串的拼接  '$变量名 $变量名' 也可 '变量名 + 变量名'
  String str1 = '你好';

  String str2 = 'Dart';

  print("$str1 $str2");

Dart 中的运算符跟js 以及其他的语言一样

        数据类型

数字类型  in double


//数字类型
//int 必须是整型
  int a = 123;

  print(a);
//double 可是整型 也可是浮点行
  double b = 23.5;
  print(b + a);

  //布尔值
  bool f = false;

  print(f);

  bool i = true;

  if (i) {
    print(i);
  }

//数组
var li = ['aa', 'bb', 'vv'];

  print(li[2]);

  var l1 = new List();
  l1.add('1');
  l1.add('2');
  l1.add('3');
  l1.add('4');

  print(l1);



//对象的定义

  var obj = {
    'name': 'Dart',
    'age': 2001,
    'sex': 'na',
    'jn': ['javasrpit', 'typescrpt', 'vue']
  };

  print(obj);
//Datr中使用对象中的值
  print(obj['age']);
  print(obj['jn']);

  var p = new Map();
  p['name'] = 'DART';
  p['sex'] = 18;
  p['wro']=['外卖','程序'];
  print(p);

// 第二种maps 定义

var ps = new Map();
ps['name']='刘忠总是';
ps['sex']=18;

print(ps)


自定义数组数据类型

var li1 = new List<String>();

li1.add('ll');
li1.add('bb');
li1.add('vv');
li1.add('ff');
li1.add('lddl');

print(li1);



var li1 = new List<Object>();

li1.add(12);
li1.add('bb');
li1.add(false);
li1.add('ff');
li1.add(123.20);

print(li1);



  //Dart 类型判断

  var st = 123;

in 关键字 判断 数据类型

  if(st is int){

    print(st is int);
  }

Dart的运算符  +加 -减 *乘 /除 %其余 ~/取整

条件运算符 ==   !=    >   <   >=    <=



